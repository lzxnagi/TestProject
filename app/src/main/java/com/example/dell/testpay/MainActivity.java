package com.example.dell.testpay;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.tencent.mobileqq.openpay.api.IOpenApi;
import com.tencent.mobileqq.openpay.api.OpenApiFactory;
import com.tencent.mobileqq.openpay.data.pay.PayApi;

import a.aa.aaa.A;
import ty.py.edit.listen.PayResultListener;
import ty.py.edit.util.Constans;



public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private Button wxPay,zfbPay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wxPay = (Button) findViewById(R.id.wx_pay);
        wxPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                A.Pay(MainActivity.this, Constans.WXPAY, "10", "vip", "", "", "1cbf8ff5-4b44-49ee-b40c-8bc8df063623", new PayResultListener() {
                    @Override
                    public void onResult(final int i) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.i(TAG, "run: "+i);
                                if (i == 100){
                                    Toast.makeText(MainActivity.this,"支付成功", Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(MainActivity.this,"支付失败",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                });
            }
        });

        zfbPay = (Button) findViewById(R.id.zfb_pay);
        zfbPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                A.Pay(MainActivity.this, 2, "10", "vip", "", "", "1cbf8ff5-4b44-49ee-b40c-8bc8df063623", new PayResultListener() {
                    @Override
                    public void onResult(final int i) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.i(TAG, "run: "+i);
                                if (i == 100){
                                    Toast.makeText(MainActivity.this,"支付成功",Toast.LENGTH_SHORT).show();
                                }else {
                                    Toast.makeText(MainActivity.this,"支付失败",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                });
            }
        });

//        IOpenApi openApi = OpenApiFactory.getInstance(this, "");
//        PayApi api = new PayApi();
//        api.appId = ""; // 在http://open.qq.com注册的AppId,参与支付签名，签名关键字key为appId
//        api.serialNumber = ""; // 支付序号,用于标识此次支付
//        api.callbackScheme = ""; // QQ钱包支付结果回调给urlscheme为callbackScheme的activity.，参看后续的“支付回调结果处理”
//        api.tokenId = ""; // QQ钱包支付生成的token_id
//        api.pubAcc = ""; // 手Q公众帐号id.参与支付签名，签名关键字key为pubAcc
//        api.pubAccHint = ""; // 支付完成页面，展示给用户的提示语：提醒关注公众帐号
//        api.nonce = ""; // 随机字段串，每次支付时都要不一样.参与支付签名，签名关键字key为nonce
//        api.timeStamp =0; // 时间戳，为1970年1月1日00:00到请求发起时间的秒数
//        api.bargainorId = ""; // 商户号.参与支付签名，签名关键字key为bargainorId
//        api.sig = ""; // 商户Server下发的数字签名，生成的签名串，参看“数字签名”
//        api.sigType = "HMAC-SHA1"; // 签名时，使用的加密方式，默认为"HMAC-SHA1"
//        if (api.checkParams()) {
//            openApi.execApi(api);
//        }
    }
}
